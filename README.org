Reproducer for crash in iris driver when gallium nine is used:

~/nine-crash.exe: ../src/gallium/drivers/iris/iris_resolve.c:670: miptree_layer_range_length: Assertion `start_layer + num_layers <= total_num_layers' failed.~

Build:

~cmake -B build64 -D CMAKE_TOOLCHAIN_FILE=mingw64.toolchain~

~make -C build64~

Fixed with: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/5808
